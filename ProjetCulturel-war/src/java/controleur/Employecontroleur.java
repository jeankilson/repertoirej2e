
package controleur;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.*;
import javax.ejb.EJB;
import modele.EmployeFacade;
import entite.*;



@Named(value = "employecontroleur")
@SessionScoped
public class Employecontroleur implements Serializable {

    @EJB
    private EmployeFacade employeFacade;

    private Employe e = new Employe();

    public Employe getE() {
        return e;
    }

    public void setE(Employe e) {
        this.e = e;
    }
    
    public Employecontroleur() {
    }
    public List<Employe> findAll()
    {
        return this.employeFacade.findAll();
    }
    
    public String AjoutEmploye()
    {
        this.employeFacade.create(this.e);
        return "AjoutEmploye";
    }
    
    public String SupEmploye(Employe e)
    {
        this.employeFacade.remove(e);
        return "Listeremploye";
    }
     public String editer(Employe e)
    {
        this.e= e;
        return "Editemp";
    }
    public String editer()
    {
        this.employeFacade.edit(this.e);
        return "index";
    }
}
