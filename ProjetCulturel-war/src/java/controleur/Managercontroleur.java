
package controleur;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.*;
import javax.ejb.EJB;
import modele.ManagerFacade;
import entite.*;


@Named(value = "managercontroleur")
@SessionScoped
public class Managercontroleur implements Serializable {

    @EJB
    private ManagerFacade managerFacade;
    private Manager m = new Manager();

    public Manager getM() {
        return m;
    }

    public void setM(Manager m) {
        this.m = m;
    }

    public Managercontroleur() {
    }
    public List<Manager> findAll()
    {
        return this.managerFacade.findAll();
    }
    
    public String SupManager(Manager m)
    {
        this.managerFacade.remove(m);
        return "ListerManager";
    }
    
    public String AjoutManager()
    {
        this.managerFacade.create(this.m);
        this.m = new Manager();
        return "ListerManager";
    }
    public String ModManager(Manager m)
    {
        this.m=m;
        return "ModiManager";
    }
    public String ModManager()
    {
        this.managerFacade.edit(this.m);
        return "ListerManager";
    }
    
}
