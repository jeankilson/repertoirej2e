
package controleur;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.*;
import javax.ejb.EJB;

import modele.ArtisteFacade;
import entite.*;


@Named(value = "artistecontroleur")
@SessionScoped
public class Artistecontroleur implements Serializable {

    @EJB
    private ArtisteFacade artisteFacade;

   private Artiste a = new Artiste();

    public Artiste getA() {
        return a;
    }

    public void setA(Artiste a) {
        this.a = a;
    }
   
    
    public Artistecontroleur() {
    }
    public List<Artiste> findAll()
    {
        return this.artisteFacade.findAll();
    }
    public String AjoutArtiste()
    {
        this.artisteFacade.create(this.a);
        this.a = new Artiste();
        return "pageprincipale";
    }
    public String SuprimeArtiste(Artiste a)
    {
        this.artisteFacade.remove(a);
        return "index";
    }
    public String edit(Artiste a)
    {
        this.a= a;
        return "Edit";
    }
    public String edit()
    {
        this.artisteFacade.edit(this.a);
        return "index";
    }
}
