
package controleur;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import modele.SalexpoFacade;
import entite.*;
import java.util.*;



@Named(value = "sallexpomanager")
@SessionScoped
public class Sallexpocontroleur implements Serializable {

    
   

    @EJB
    private SalexpoFacade salexpoFacade;
    private Salexpo s = new Salexpo();

    public Salexpo getS() {
        return s;
    }

    public void setS(Salexpo s) {
        this.s = s;
    }
    

   
    public Sallexpocontroleur() {
    }
    public List<Salexpo> findAll()
    {
        return this.salexpoFacade.findAll();
    }
    
    public String SupSalle(Salexpo s)
    {
        this.salexpoFacade.remove(s);
        return "ListerSalle";
    }
    
     public String ModSal(Salexpo s)
    {
        this.s=s;
        return "ListerSalle";
    }
    public String ModSal()
    {
        this.salexpoFacade.edit(this.s);
        return "ListerSalle";
    }
    
     public String AjoutSalle()
    {
        this.salexpoFacade.create(this.s);
        this.s = new Salexpo();
        return "ListerSalle";
    }
}
