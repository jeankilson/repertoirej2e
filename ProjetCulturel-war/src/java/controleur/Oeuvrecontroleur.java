
package controleur;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.*;
import javax.ejb.EJB;
import modele.OeuvreFacade;
import entite.*;

@Named(value = "oeuvrecontroleur")
@SessionScoped
public class Oeuvrecontroleur implements Serializable {

    @EJB
    private OeuvreFacade oeuvreFacade;

    private Oeuvre o = new Oeuvre();

    public Oeuvre getO() {
        return o;
    }

    public void setO(Oeuvre o) {
        this.o = o;
    }
    
    public Oeuvrecontroleur() {
    }
    public List<Oeuvre> findAll()
    {
        return this.oeuvreFacade.findAll();
    }
    
    public String AjoutOeuvre()
    {
        this.oeuvreFacade.create(this.o);
        return "ListerOeuvre";
    }
     public String SupOeuvre(Oeuvre o)
    {
        this.oeuvreFacade.remove(o);
        return "ListerOeuvre";
    }
     
      public String Modifier(Oeuvre o)
    {
        this.o= o;
        return "ModOeuvre";
    }
    public String Modifier()
    {
        this.oeuvreFacade.edit(this.o);
        return "ListerOeuvre";
    }
}
